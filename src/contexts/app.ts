import { createContext } from 'react';

interface IContextProps {
    authenticated: boolean,
    theme: string
}

const { Provider, Consumer } = createContext<Partial<IContextProps>>({
    authenticated: true,
    theme: 'dark'
});

export { Provider, Consumer };