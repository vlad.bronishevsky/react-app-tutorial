export enum CounterActions {
    INCREMENT = 'INCREMENT',
}

export enum ArticleActions {
    DELETE = 'DELETE',
    FETCH_STARTED = 'FETCH_STARTED',
    FETCHED = 'FETCHED',
    FETCH_FAILED = 'FETCH_FAILED'
}

export const SEARCH = 'SEARCH';