import { ActionCreator } from "redux";
import * as actions from "./actions";

export interface ISearchAction {
    type: typeof actions.SEARCH;
    searchTerm: string;
}

export const search: ActionCreator<ISearchAction> = (searchTerm: string) => {
    return {
        type: actions.SEARCH,
        searchTerm
    }
};