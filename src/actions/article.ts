import { ActionCreator, Dispatch } from "redux";
import { ArticleActions } from "./actions";
import { IArticle } from "../components/Article";
import { CallHistoryMethodAction, replace } from "connected-react-router";

export interface IArticleAction {
    type: ArticleActions;
    payload: Partial<IArticleActionPayload>;
}

interface IArticleActionPayload {
    url: string,
    data: Array<IArticle>,
    error: Error
}

export const deleteArticle: ActionCreator<IArticleAction> = (url: string) => {
    return {
        type: ArticleActions.DELETE,
        payload: { url }
    }
};

export const loadArticles = () => {
    return (dispatch: Dispatch<IArticleAction | CallHistoryMethodAction>) => {
        dispatch({
            type: ArticleActions.FETCH_STARTED,
            payload: { data: [] },
        });

        const request = new Request('https://newsapi.org/v2/everything?q=bitcoin&apiKey=0e404a16315d4435ada29dfe8d41f3d0');
        fetch(request).then((response) => response.json())
            .then((data) => dispatch({
                type: ArticleActions.FETCHED,
                payload: { data: data.articles }
            }))
            .catch((error) => {
                dispatch(replace('/error'));

                dispatch({
                    type: ArticleActions.FETCH_FAILED,
                    payload: { error }
                })
            });
    };
};