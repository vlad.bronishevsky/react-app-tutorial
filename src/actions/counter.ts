import { ActionCreator } from "redux";
import { CounterActions } from "./actions";

export interface ICounterAction {
    type: CounterActions;
}

export const increment: ActionCreator<ICounterAction> = () => {
    return {
        type: CounterActions.INCREMENT
    }
};