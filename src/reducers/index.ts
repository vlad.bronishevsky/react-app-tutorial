import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import countReducer, { CounterState } from './counter';
import articleReducer, { IArticlesState } from './article';
import searchReducer, { SearchState } from "./search";
import history from '../history';

export interface IStoreState {
    counter: CounterState;
    articles: IArticlesState,
    searchTerm: SearchState
}

export default combineReducers({
    count: countReducer,
    articles: articleReducer,
    searchTerm: searchReducer,
    router:  connectRouter(history),
});