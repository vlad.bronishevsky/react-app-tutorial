import { ArticleActions, IArticleAction } from "../actions";
import { IArticle } from "../components/Article";

export interface IArticlesState {
    data: Array<IArticle>,
    isLoading: boolean
};

const defaultState = { data: [], isLoading: false };

export default (state: IArticlesState = defaultState, action: IArticleAction) => {
    switch (action.type) {
        case ArticleActions.DELETE:
            return {
                ...state,
                data: state.data.filter((article: IArticle) => article.url !== action.payload.url),
            };

        case ArticleActions.FETCH_STARTED:
            return {
                ...state,
                isLoading: true,
            };

        case ArticleActions.FETCHED:
        case ArticleActions.FETCH_FAILED:
            return {
                ...state,
                ...action.payload,
                isLoading: false,
            };

        default:
            return state;
    }
}