import { CounterActions, ICounterAction } from "../actions";

export type CounterState = number;

export default (state: number = 0, action: ICounterAction) => {
    switch (action.type) {
        case CounterActions.INCREMENT: {
            return state + 1;
        }
    }
    return state;
}