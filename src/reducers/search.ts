import { ArticleActions, SEARCH, IArticleAction, ISearchAction } from "../actions";

export type SearchState = string;

export default (state: SearchState = '', action: ISearchAction | IArticleAction) => {
    switch (action.type) {
        case ArticleActions.DELETE:
            return '';

        case SEARCH:
            const searchTerm = action.searchTerm;
            return searchTerm;

        default:
            return state;
    }
}