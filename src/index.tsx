import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import ErrorBoundary from './components/ErrorBoundary';
import App from './components/App';
import store from './store';
import { Provider } from 'react-redux';
import { Provider as AppProvider } from './contexts';
import * as serviceWorker from './serviceWorker';
import { ConnectedRouter } from "connected-react-router";
import history from "./history";
import LangProvider from "./components/i18n/LanguageProvider";

ReactDOM.render(
    <AppProvider value={{
        authenticated: false,
        theme: 'light'
    }}>
        <LangProvider language={'ru'}>
            <Provider store={ store }>
                <ConnectedRouter history={ history }>
                    <ErrorBoundary>
                        <App />
                    </ErrorBoundary>
                </ConnectedRouter>
            </Provider>
        </LangProvider>
    </AppProvider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
