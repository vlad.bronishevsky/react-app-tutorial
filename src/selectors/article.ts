import { createSelector } from 'reselect';
import { IStoreState } from "../reducers";
import { IArticle } from "../components/Article";

const articlesSelector = (state: IStoreState) => state.articles.data;
const searchTermSelector = (state: IStoreState) => state.searchTerm;

export const searchedArticlesSelector = createSelector(articlesSelector, searchTermSelector,
    (articles, searchTerm) => {
        const searchTermInLowerCase = searchTerm.toLowerCase();
        return {
            articles: searchTermInLowerCase === '' ?
                articles :
                articles.filter((article: IArticle) =>
                    article.description.toLowerCase().includes(searchTermInLowerCase) ||
                    article.title.toLowerCase().includes(searchTermInLowerCase))
        };
});

const urlSelector = (state: IStoreState, props: {url?: string}) => props.url;

export const articleSelector = createSelector(
    articlesSelector,
    urlSelector,
    (articles, url) => articles.find(article => encodeURIComponent(article.url) === url)
);
