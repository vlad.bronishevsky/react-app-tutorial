import './Loader.scss';
import React, { FC } from 'react';

export const Loader: FC = () => {
    return <div className="heart-loader"><div></div></div>;
};