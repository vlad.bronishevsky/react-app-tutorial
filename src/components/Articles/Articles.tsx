import React, { Component } from 'react';
import Article from "../Article";
import ArticlesList from "../ArticlesList";
import { Route, RouteComponentProps } from "react-router";
import i18n, {Ii18nProps} from '../i18n';

class Articles extends Component<Ii18nProps> {
    public render() {
        return (
            <>
                <ArticlesList/>
                <Route path="/articles" render={ this.renderPlaceholder } exact/>
                <Route path="/articles/:id" render={ this.renderArticle } />
            </>
        )

    }

    private renderPlaceholder = () => {
        return <h2>{ this.props.t('Please select an Article!') }</h2>;
    };

    private renderArticle = ({ match }: RouteComponentProps<{id?: string}>) => {
        const id = match.params.id;
        return <Article key={ id } url={ id } />;
    };
}

export default i18n(Articles);