import React, { Component } from 'react';
import toggleDisplay,  { IToggleDisplayProps } from '../toggleDisplay';
import './Article.scss';
import { connect } from "react-redux";
import { IStoreState } from "../../reducers";
import { articleSelector } from "../../selectors";

export interface IArticle {
    description: string,
    urlToImage: string,
    title: string,
    author: string,
    url: string
}

interface IStateProps {
    article?: IArticle
}

interface IOwnProps {
    url?: string
}

type IArticleProps = IStateProps & IToggleDisplayProps;

class Article extends Component<IArticleProps> {
    render() {
        const { article } = this.props;

        if (!article) {
            return null;
        }

        const content = this.props.isDisplayed ?
            (
                <div>
                    <p>{article.description}</p>
                    <img className="preview-image" src={article.urlToImage} alt='article preview'/>
                </div>
            ) : null;

        const toggleSymbol = this.props.isDisplayed ? '▲' : '▼';

        return (
            <article className="article">
                <h2>{article.title}</h2>
                <h4>{article.author}</h4>
                <span className='toggle-display-button'
                      onClick={this.props.toggleDisplayed}>{toggleSymbol}
                </span>
                {content}
            </article>
        );
    }
}

export default connect(
    (state: IStoreState, props: IOwnProps) => ({
        article: articleSelector(state, props)
    })
)(toggleDisplay(Article));
