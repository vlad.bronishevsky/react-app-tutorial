import React from 'react';

export interface ISearchBoxProps {
    searchTerm: string,
    handleSearch: (event: React.ChangeEvent<HTMLInputElement>) => void
}

const SearchBox = React.forwardRef<HTMLInputElement, ISearchBoxProps>((props, ref) => {
    return  <input
        ref={ ref }
        value={ props.searchTerm }
        onChange={ props.handleSearch }
    />
});

export default SearchBox;