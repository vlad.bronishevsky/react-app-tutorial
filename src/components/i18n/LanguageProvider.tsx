import React, { Component } from 'react';
import dictionaries from './dictionaries';
import { Provider } from './context';

interface IPropTypes {
    language: string;
}

class LangProvider extends Component<IPropTypes> {
    public render() {
        return <Provider value={ dictionaries[this.props.language] }>
            { this.props.children }
        </Provider>
    }
}

export default LangProvider;