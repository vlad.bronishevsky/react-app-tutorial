import React from 'react';
import { Consumer } from './context';
import { Dictionary } from './dictionaries';

export interface Ii18nProps {
    t: (text: string) => string,
}

export default <T extends Ii18nProps>(OriginalComponent: React.ComponentType<T>) =>
    class Translate extends React.Component<common.Subtract<T, Ii18nProps>> {
        private translate = (dictionary: Dictionary) => (text: string) => dictionary[text] || text;

        public render() {
            return (
                <Consumer>
                    {(dictionary: Dictionary) =>
                        <OriginalComponent { ...this.props as T } t={ this.translate(dictionary) }/>
                    }
                </Consumer>
            )
        }
    }