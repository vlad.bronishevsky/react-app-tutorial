export type Dictionary = Record<string, string>;

export const en: Dictionary = {
    'Increment me': 'Increment me',
    Articles: 'Articles',
    Counter: 'Counter',
    Delete: 'Delete',
    'Common Layout. This is my location:': 'Common Layout. This is my location:',
    'Please, sign in': 'Please, sign in',
    'Logged in!': 'Logged in!',
    'Please select an Article!': 'Please select an Article!',
};

export const ru: Dictionary = {
    'Increment me': 'Увеличь меня',
    Articles: 'Статьи',
    Counter: 'Счетчик',
    Delete: 'Удалить',
    'Common Layout. This is my location:': 'Общий макет. Я сейчас здесь:',
    'Please, sign in': 'Пожалуйста, зарегистрируйтесь',
    'Logged in!': 'Залогинен!',
    'Please select an Article!': 'Пожалуйста, выберите статью!',
};

const dictionaries: Record<string, Dictionary> = { ru, en };

export default dictionaries;