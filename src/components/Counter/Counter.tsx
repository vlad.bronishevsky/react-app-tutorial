import React, { Component } from 'react';
import { connect } from 'react-redux';
import { increment } from '../../actions';
import i18n, { Ii18nProps } from '../i18n'

interface IState { count: number }
interface IStateProps { count: number }
interface IDispatchProps { increment: () => void }

type IProps = IStateProps & IDispatchProps & Ii18nProps;

export class Counter extends Component<IProps>  {
    handleIncrement = () => {
         // this.props.dispatch(increment());
         this.props.increment();
    };

    render() {
        const { count, t } = this.props;

        return (
            <div>
                <h2>{ count }</h2>
                <button onClick={ this.handleIncrement }>{ t('Increment me') }</button>
            </div>
        );
    }
}

function mapStateToProps({ count }: IState): IStateProps {
    return {
        count
    }
}

const mapDispatchToProps = { increment };

export default i18n(connect<IStateProps, IDispatchProps, {}, IState>
   (mapStateToProps, mapDispatchToProps)(Counter));