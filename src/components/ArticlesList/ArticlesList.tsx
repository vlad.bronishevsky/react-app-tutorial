import React, {Component, FC} from 'react';
import { IArticle } from "../Article";
import { connect } from 'react-redux';
import { IStoreState } from "../../reducers";
import { searchedArticlesSelector } from "../../selectors";
import { NavLink, RouteComponentProps, withRouter } from "react-router-dom";
import { deleteArticle } from "../../actions";
import i18n, { Ii18nProps } from '../i18n';

interface IStateProps {
    articles: IArticle[],
}

interface IDispatchProps {
    deleteArticle: (url: string) => void
};

type IArticlesListProps = IStateProps & IDispatchProps & Ii18nProps & RouteComponentProps;

class ArticlesList extends Component<IArticlesListProps> {
    private handleDelete = (url: string) => {
        const { deleteArticle } = this.props;
        deleteArticle(url);
    };

    public render() {
        const { articles, t } = this.props;
        return <ul>
            {
                articles.map((article) =>
                    <li key={ article.url }>
                        <button  onClick={() => this.handleDelete(article.url)}>{ t('Delete') }</button>
                        <NavLink to={`/articles/${encodeURIComponent(article.url)}`} activeStyle={{color: 'green'}}>
                            { article.title }
                        </NavLink>
                    </li>
                )
            }
        </ul>
    }
};

export default i18n(withRouter(connect((state: IStoreState) => {
    return searchedArticlesSelector(state);
},
    { deleteArticle }
)(ArticlesList)));
