import React, { Component, ReactNode } from 'react';

interface IErrorBoundaryState {
    error: Error | null
}

export default class ErrorBoundary extends Component<{}, IErrorBoundaryState> {
    constructor(props: {}) {
        super(props);
        this.state = { error: null };
    }

    public static getDerivedStateFromError(error: Error): Partial<IErrorBoundaryState> {
        // Update state so the next render will show the fallback UI.
        return { error };
    }

    public componentDidCatch(error: Error, errorInfo: {}): void {
        // Catch errors in any components below and re-render with error message
        // log(errorInfo)
        // You can also log error messages to an error reporting service here
    }

    public render(): ReactNode {
        if (this.state.error) {
            return (
                <div>
                    <h2>Something went wrong.</h2>
                    <details>
                        {this.state.error.toString()}
                    </details>
                </div>
            );
        }
        // Normally, just render children
        return this.props.children;
    }
}