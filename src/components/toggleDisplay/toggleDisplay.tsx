import React, { Component, ReactNode } from 'react';

type Subtract<T, K> = common.Subtract<T, K>;

interface IToggleDisplayState {
    isDisplayed: boolean
}

export interface IToggleDisplayProps {
    isDisplayed: boolean,
    toggleDisplayed: () => void
}

export default <T extends IToggleDisplayProps>(OriginalComponent: React.ComponentType<T>) =>
    class WrappedComponent extends Component<
        Subtract<T, IToggleDisplayProps>,
        IToggleDisplayState
        > {
            public state = {
                isDisplayed: false
            };

            private toggleDisplayed = () => {
                this.setState((state) => ({
                    isDisplayed: !state.isDisplayed
                }));
            };

            public render(): ReactNode {
                return <OriginalComponent
                    { ...this.props as T }
                    isDisplayed={ this.state.isDisplayed }
                    toggleDisplayed={ this.toggleDisplayed }
                />
            }
}