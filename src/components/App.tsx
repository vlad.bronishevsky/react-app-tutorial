import React, { Component, ReactNode } from 'react';
import Articles from "./Articles";
import SearchBox from "./SearchBox";
import Counter from "./Counter";
import { connect } from "react-redux";
import { IStoreState } from "../reducers";
import { loadArticles, search } from "../actions";
import { Loader } from "./Loader/Loader";
import { Route, Switch, Link, Redirect, withRouter, RouteComponentProps } from 'react-router-dom';
import { Consumer as AppConsumer } from '../contexts';
import i18n, { Ii18nProps } from '../components/i18n';


// These props are provided when creating the component
interface IOwnProps {
    // ...
}

// These props are provided via connecting the component to the store via mapDispatchToProps
interface IDispatchProps {
    search: (searchTerm: string) => void,
    loadArticles: () => void,
}

// These props are provided via connecting the component to the store via mapStateToProps
interface IStateProps {
    searchTerm: string,
    isLoading: boolean
}

// These props are provided by the router
interface IPathProps {
    // ...
}

type IAppProps = IOwnProps & IStateProps & IDispatchProps & Ii18nProps & RouteComponentProps<IPathProps>;

class App extends Component<IAppProps> {
    private searchBoxRef = React.createRef<HTMLInputElement>();

    public componentDidMount() {
        if (!this.props.isLoading) {
            this.props.loadArticles();
        }
        this.searchBoxRef.current && this.searchBoxRef.current.focus();
    };

    private handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
        const searchTerm = event.target.value;
        this.props.search(searchTerm);
    };

    public renderLoader() {
        return <Loader/>;
    }

    public renderApp() {
        const { location, t } = this.props;
        return <div>
            <div>
                <div>
                    <Link to="/counter">{ t('Counter') }</Link>
                </div>
                <div>
                    <Link to="/articles">{ t('Articles') }</Link>
                </div>
            </div>
            <AppConsumer>{
                ({authenticated}) => {
                    const layout = <div>
                        <span>{ `${ t('Common Layout. This is my location:') } ${ location.pathname }` }</span>
                    </div>;
                    return <>
                        <h1>{ authenticated ? t('Logged in!') : t('Please, sign in')}</h1>
                        { layout }
                    </>;
                }
            }
            </AppConsumer>
            <Switch>
                <Redirect from="/" to="/articles" exact />
                <Route path="/counter" component={ Counter }/>
                <Route path="/articles/new" render={() => <h2>{ t('Add New Article') }</h2>}/>
                <Route path="/articles" render={ this.renderSearchAndArticles }/>
                <Route path="/error" render={() => <h1>{ t('Oops! An error occurred!') }</h1>} />
                <Route path="*" render={() => <h1>{ t('Oops! Not Found ...') }</h1>}/>
            </Switch>
        </div>
    }

    renderSearchAndArticles = () => {
        return <>
            <SearchBox
                ref={ this.searchBoxRef }
                searchTerm={ this.props.searchTerm }
                handleSearch={ this.handleSearch }
            />
            <Articles/>
        </>
    };

    public render(): ReactNode {
        return this.props.isLoading ? this.renderLoader() : this.renderApp();
    };
}


export default i18n(withRouter(connect<IStateProps, IDispatchProps, IOwnProps, IStoreState>(
    ({ searchTerm, articles }: IStoreState) => ({
        searchTerm,
        isLoading: articles.isLoading
    }),
    { search, loadArticles }
)(App)));