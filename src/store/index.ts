import { createStore, applyMiddleware } from 'redux';
import reducer from '../reducers';
import { logger } from "../middlewares/logger";
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from "redux-thunk";
import { routerMiddleware } from 'connected-react-router';
import history from '../history';

const enhancer = composeWithDevTools(applyMiddleware(thunk, routerMiddleware(history), logger));
const store = createStore(reducer, enhancer);

export default store;